<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\FiveCharacters;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => new FiveCharacters(),
            'price' => 'required|numeric',
            'avatar' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'  => 'A name attribute is required',
            'price.required' => 'A price attribute is required',
            'avatar.required' => 'A avatar attribute is required',
        ];
    }
}
