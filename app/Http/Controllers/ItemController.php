<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ItemRequest;
use App\Rules\FiveCharacters;

class ItemController extends Controller
{
    public function create()
    {
        return view('items');
    }

    public function store(ItemRequest $request)
    {
        $request->validate();
        return $this->show();
    }

    public function show(){
        return 'validation complete';
    }
}
